# CT__kss-node-demo

## Demo

To install the demo:

1. Check that you have the latest version of [Node.js](https://nodejs.org/download/) installed. This demo has been tested with Node `0.12.2` (`node -v`) and NPM `2.7.4` (`npm -v`)
1. `git clone https://bitbucket.org/chrometoaster/ct__kss-node-demo.git#v1.0.0`
1. `cd ct__kss-node-demo`
1. `npm install`
1. `grunt`
1. `open ct-styleguide/index.html`
1. Read the [ct__kss-node-template documentation](https://bitbucket.org/chrometoaster/ct__kss-node-template).

## KSS 2.0.2 (March 2015)

Reviewing a major upgrade from `0.3.6` to `2.0.2`.

### New features

#### Modifier placeholder changed

* `{$modifiers}` now `{{modifier_class}}` ([2.0.0-alpha.1](https://github.com/kss-node/kss-node/releases/tag/v2.0.0-alpha.1))
* the (old) `{$modifiers}` code is no longer output as `{$modifiers}` if it is not used, however a space __is__ output - this can be suppressed with `{{#if modifier_class}} {{modifier_class}}{{/if}}` conditional in the Handlebars partials

#### Handlebars partials now supported

* `Markup: nameofpartial.hbs` can now be used to specify the KSS markup (note that the `.mustache` extension is not supported)
* `nameofpartial.json` is automatically used to populate placeholders in `nameofpartial.hbs` ([2.0.2](https://github.com/kss-node/kss-node/releases/tag/v2.0.2))
* `.js` helpers can be created to manipulate the output of placeholders ([1.1.0](https://github.com/kss-node/kss-node/releases/tag/v1.1.0))

#### Custom templates are easier to create

* custom templates can bow be created using `kss-node --init CUSTOM_TEMPLATE_NAME`
* `--custom` template properties can be used to easily change the styleguide layout ([2.0.0-beta.1](https://github.com/kss-node/kss-node/releases/tag/v2.0.0-beta.1))
* note that the contents of `CUSTOM_TEMPLATE_NAME/public` are automatically copied to the generated styleguide's `public` folder, hence `.bowerrc`

#### Portable configuration added

* a `json` config file can be used to control the `kss-node` options ([1.1.0](https://github.com/kss-node/kss-node/releases/tag/v1.1.0), [a802c2215b0b41f75798a5472831ea2fe42937b8](https://github.com/kss-node/kss-node/commit/a802c2215b0b41f75798a5472831ea2fe42937b8))

#### Styleguide numbering simplified

* `autoincrement`ing text-based indices can now be used, eg `Styleguide buttons.special` vs `Styleguide 3.14` (refer `kss/demo/components/buttons.less`) ([2.0.0-beta.1](https://github.com/kss-node/kss-node/releases/tag/v2.0.0-beta.1))
  * indices are alphabetically sorted by the `.index` name
  * you can use the new `Weight` property to weight an entry so it appears out-of-order: `Weight: -1` ([2.0.0-alpha.4](https://github.com/kss-node/kss-node/releases/tag/v2.0.0-alpha.4), [issue 142](https://github.com/kss-node/kss-node/issues/142))

#### Pseudo states now display

* pseudo state previews like `:hover` are now working in Chrome when the styleguide is viewed on a webserver

---

### Upgrading

#### Uninstall the old version

This prevents the old global installation from being used despite a newer local copy being installed

        npm uninstall -g kss

---

### Tutorials

#### Creating a custom template

        node_modules/.bin/kss-node --init CUSTOM_TEMPLATE_NAME

#### Adding some sub templating options using the new `--custom` properties

This would be useful for outputting different markup around the styleguide demos, for example a `compact` layout for small button components, an `expanded` layout for larger more complex modules, and some sort of repetitive output to demonstrate different wrappers or breakpoints (see also the conversation @ https://github.com/kss-node/kss-node/issues/164).

1. Open `CUSTOM_TEMPLATE_NAME/index.html`
1. Add some custom markup (src: https://github.com/kss-node/kss-node/issues/124)

        <!-- CUSTOM_TEMPLATE_NAME/index.html -->
        {{#if subtemplate}}subtemplate = {{subtemplate}}{{/if}}
        {{#if subsubtemplate}}subsubtemplate = {{subsubtemplate}}{{/if}}

1. Populate the custom property values in the CSS:
        /*
        Submit Button

        Note: The space between 'subtemplate:' and 'compact' is essential, and could also be replaced with a line break.

        Markup:
        <input type="submit" class="b-submit {{modifier_class}}" value="Submit" />

        subtemplate: compact

        subsubtemplate: 3breakpoints

        .somemodifier - some style variation

        Styleguide 1.1
        */

#### Creating a KSS configuration file to store CLI options

This saves typing :)

From https://github.com/kss-node/kss-node/blob/master/test/cli-option-config.json.

The config file specifies the following options:

1. a `source` folder (string) or folders (array of strings) containing the KSS commented CSS files
1. a `destination` folder name (string) for the styleguide
1. a custom `template` folder name (created using `node_modules/.bin/kss-node --init CUSTOM_TEMPLATE_NAME`)
1. a `css` file (string) or files (array of strings) to attach to the generated styleguide (this path needs to be relative to the styleguide output)
1. a `js` file (string) or files (array of strings) to attach to the generated styleguide (this path needs to be relative to the styleguide output)
1. a `custom` property (string) or properties (array of strings) you've used

        # ./ct-kss-config.json
        # node_modules/.bin/kss-node SOURCE DESTINATION --template TEMPLATE --css .CSS --custom CUSTOM

        {
            "source": "ct-styles",
            "destination": "ct-styleguide",
            "template": "ct-template",
            "css": "../ct-styles/buttons.css",
            "css": "../ct-scripts/script.js",
            "custom": "ct-layout"
        }
