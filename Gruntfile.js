module.exports = function(grunt) {

  // JIT(Just In Time) plugin loader for Grunt.
  require('jit-grunt')(grunt);

  // Display the elapsed execution time of grunt tasks
  require('time-grunt')(grunt);

  grunt.initConfig({

    subgrunt: {
      kss_node: {
        options: {
          npmClean: false,
          npmInstall: false,
          passGruntFlags: false
        },
        projects: {
          "node_modules/ct__kss-node-template": ["default"]
        }
      }
    }

  });

  // Tasks

  grunt.registerTask("styleguide", [
    "subgrunt:kss_node"
  ]);

  grunt.registerTask("styles", [
    "styleguide"
  ]);

  grunt.registerTask('default', ['styles']);
};